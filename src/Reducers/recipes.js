import {
  GET_RECIPES,
  FAVOURITE_RECIPE,
  GET_CURRENT_RECIPE,
  SET_ALL,
  RATE_RECIPE,
} from '../Actions/';

const initialState = {
  recipes: [],
  currentRecipe: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_RECIPES:
      return {
        ...state,
        recipes: action.payload,
      };
    case FAVOURITE_RECIPE:
    case RATE_RECIPE:
    case SET_ALL:
      return {
        ...state,
        ...action.payload,
      };
    case GET_CURRENT_RECIPE:
      return {
        ...state,
        currentRecipe: action.payload,
      };
    default:
      return state;
  }
};
