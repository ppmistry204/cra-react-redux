import axios from 'axios';

const headers = {};

const addInterceptors = () => {
  // Add any interceptor here
  axios.interceptors.request.use(
    config => {
      // DO SOMETHING WITH REQUEST
      return config;
    },
    err => {
      // DO SOMETHING WITH ERROR
      return Promise.reject(err);
    }
  );
};

const createRequest = config => {
  // Add any common headers and other stuff here
  // Add some base URL here

  return axios({
    ...config,
    headers: headers,
  });
};

const get = (url, params = {}) => {
  const reqConfig = {
    method: 'GET',
    url,
    params,
  };
  return createRequest(reqConfig);
};

const post = (url, data = {}, params = {}) => {
  const reqConfig = {
    method: 'POST',
    url,
    data,
    params,
  };
  return createRequest(reqConfig);
};

const put = (url, data = {}, params = {}) => {
  const reqConfig = {
    method: 'PUT',
    url,
    data,
    params,
  };
  return createRequest(reqConfig);
};

const del = (url, data = {}, params = {}) => {
  const reqConfig = {
    method: 'DELETE',
    url,
    data,
    params,
  };
  return createRequest(reqConfig);
};

addInterceptors();

export { get, post, put, del };
