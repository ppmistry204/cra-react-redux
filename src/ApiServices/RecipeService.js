import { get } from './../ApiServices/RequestService';

const getRecipes = () => {
  const url =
    'https://s3-eu-west-1.amazonaws.com/frontend-dev-test/recipes.json';
  return get(url);
};

export { getRecipes };
