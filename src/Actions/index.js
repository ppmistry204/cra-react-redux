export const GET_RECIPES = 'recipes/GET_RECIPES';
export const FAVOURITE_RECIPE = 'recipe/FAVOURITE_RECIPE';
export const RATE_RECIPE = 'recipe/RATE_RECIPE';
export const GET_CURRENT_RECIPE = 'recipe/GET_CURRENT_RECIPE';
export const SET_ALL = 'recipes/SET_ALL';
