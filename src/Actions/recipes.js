import {
  GET_RECIPES,
  FAVOURITE_RECIPE,
  GET_CURRENT_RECIPE,
  SET_ALL,
  RATE_RECIPE,
} from './';
import { getRecipes } from '../ApiServices/RecipeService';
import { find } from 'lodash';

const fetchRecipes = () => {
  return (dispatch, getState) => {
    const { recipes } = getState().recipes;
    if (!recipes || !recipes.length) {
      return getRecipes()
        .then(resp => {
          // Randomly setting favourited recipes.
          const recipes = resp.data.map(recp => {
            recp.isFavourite = Math.random() < 0.3;
            return recp;
          });
          dispatch({
            type: GET_RECIPES,
            payload: recipes,
          });
        })
        .catch(error => {
          throw error;
        });
    }
  };
};

const fetchCurrentRecipe = id => {
  return (dispatch, getState) => {
    let recipes = getState().recipes.recipes;
    let currentRecipe = null;

    if (recipes && recipes.length) {
      currentRecipe = find(recipes, { id }) || null;
      if (currentRecipe) {
        dispatch({
          type: GET_CURRENT_RECIPE,
          payload: currentRecipe,
        });
        return;
      }
    }
    return getRecipes()
      .then(resp => {
        // Randomly setting favourited recipes.
        const recipes = resp.data.map(recp => {
          recp.isFavourite = Math.random() < 0.3;
          if (recp.id === id) {
            currentRecipe = recp;
          }
          return recp;
        });
        if (!currentRecipe) {
          throw new Error({ message: 'not found' });
        }
        dispatch({
          type: SET_ALL,
          payload: {
            recipes,
            currentRecipe,
          },
        });
      })
      .catch(err => {
        if (err.message === 'not found') {
          // Handle not found
        }
      });
  };
};

const favouriteRecipe = (recipe, fav) => {
  return (dispatch, getState) => {
    const { recipes, currentRecipe } = getState().recipes;
    const payload = {};

    const newRecipes = recipes.map(recp => {
      if (recp.id === recipe.id) {
        recp.isFavourite = fav;
      }
      return recp;
    });
    payload.recipes = newRecipes;

    if (currentRecipe && currentRecipe.id === recipe.id) {
      const updatedCurrRecipe = {
        ...currentRecipe,
        isFavourite: fav,
      };
      payload.currentRecipe = updatedCurrRecipe;
    }

    console.log('Simulating network for fave. ');
    setTimeout(() => {
      dispatch({
        type: FAVOURITE_RECIPE,
        payload,
      });
    }, 500);
  };
};

const getNewRating = (avgRating, currentRating) => {
  if (currentRating) {
    return (avgRating + currentRating) / 2;
  } else {
    return avgRating;
  }
};

const rateRecipe = (recipeId, rating) => {
  return (dispatch, getState) => {
    const { recipes, currentRecipe } = getState().recipes;
    const payload = {};

    const newRecipes = recipes.map(recp => {
      if (recp.id === recipeId) {
        recp.currentRating = rating;
        recp.newRating = getNewRating(recp.rating, rating);
      }
      return recp;
    });
    payload.recipes = newRecipes;

    if (currentRecipe && currentRecipe.id === recipeId) {
      const updatedCurrRecipe = {
        ...currentRecipe,
        currentRating: rating,
        newRating: getNewRating(currentRecipe.rating, rating),
      };
      payload.currentRecipe = updatedCurrRecipe;
    }

    console.log('Simulating network for fave. ');
    setTimeout(() => {
      dispatch({
        type: RATE_RECIPE,
        payload,
      });
    }, 500);
  };
};

export { fetchRecipes, favouriteRecipe, fetchCurrentRecipe, rateRecipe };
