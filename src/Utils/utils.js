const replaceSpaceInString = (str, replacement) => {
  const replaceChar = replacement || '-';
  const spaceRegex = / /g;
  return str.trim().replace(spaceRegex, replaceChar);
};

const formatPrepTime = time => {
  const timeRegex = /PT\d*M/gi;
  if (time.match(timeRegex)) {
    const prepTimeRegex = /PT/gi;
    const mRegex = /M/gi;
    return parseInt(time.replace(prepTimeRegex, '').replace(mRegex, ''), 10);
  } else {
    return 0;
  }
};

export { replaceSpaceInString, formatPrepTime };
