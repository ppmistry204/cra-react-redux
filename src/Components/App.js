import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import RecipesComponent from './ContainerComponents/RecipesComponent';
import RecipeDetailComponent from './ContainerComponents/RecipeDetailComponent';
import Nav from './CoreComponents/Nav';

/*
 We encourage our candidates to over-engineer,
 so please feel free to use any other styling methodology
 e.g., Emotion, Fela, SASS, etc.
 */

const DemoPage = () => {
  return <h3>Welcome to HB-CRM</h3>;
};

class App extends React.Component {
  render() {
    return (
      <Router>
        <React.Fragment>
          <Nav />
          <div className="grid-container main-content">
            <React.Fragment>
              <Route path="/" exact component={DemoPage} />
              <Route path="/recipes" exact component={RecipesComponent} />
              <Route
                path="/recipe/:name/:id"
                exact
                component={RecipeDetailComponent}
              />
            </React.Fragment>
          </div>
        </React.Fragment>
      </Router>
    );
  }
}

export default App;
