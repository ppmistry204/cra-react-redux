import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import {
  fetchRecipes,
  favouriteRecipe,
  rateRecipe,
} from '../../../Actions/recipes';
import RecipeCard from '../../CoreComponents/RecipeCard/index';

class RecipesComponent extends React.Component {
  componentDidMount() {
    this.props.fetchRecipes();
  }

  createRecipeList = () => {
    const { recipes, rateRecipe, favouriteRecipe } = this.props;
    return recipes.map(recipe => (
      <div className="sm-12 md-6 lg-4 column" key={recipe.id}>
        <RecipeCard
          recipe={recipe}
          onFavouriting={favouriteRecipe}
          onRating={rateRecipe}
        />
      </div>
    ));
  };

  render() {
    return (
      <React.Fragment>
        <h2>Recipes</h2>
        <div className="recipe-container row">{this.createRecipeList()}</div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => ({
  recipes: state.recipes.recipes,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchRecipes,
      favouriteRecipe,
      rateRecipe,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RecipesComponent);
