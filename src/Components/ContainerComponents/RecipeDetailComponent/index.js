import React from 'react';
import { withRouter } from 'react-router';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  favouriteRecipe,
  fetchCurrentRecipe,
  rateRecipe,
} from '../../../Actions/recipes';
import { formatPrepTime } from '../../../Utils/utils';
import Panel from '../../CoreComponents/Panel/index';
import FavouriteMe from '../../CoreComponents/FavouriteMe/index';
import Badge from '../../CoreComponents/Badge/index';
import RateMe from '../../CoreComponents/RateMe/index';

class RecipeDetailComponent extends React.Component {
  constructor(props) {
    super(props);
    this.recipeId = props.match.params.id;
  }

  componentDidMount() {
    this.props.fetchCurrentRecipe(this.recipeId);
  }

  getNutritionValue() {
    const { calories, carbos, fats, proteins } = this.props.currentRecipe;
    const nutritionValues = [];
    if (calories) nutritionValues.push({ nutrient: 'Calories', val: calories });
    if (carbos) nutritionValues.push({ nutrient: 'Carbs', val: carbos });
    if (fats) nutritionValues.push({ nutrient: 'Fats', val: fats });
    if (proteins) nutritionValues.push({ nutrient: 'Proteins', val: proteins });

    return nutritionValues.map((n, index) => (
      <div className="row" key={index}>
        <div className="record">
          <span className="key">{n.nutrient}</span>
          <span className="value">{n.val}</span>
        </div>
      </div>
    ));
  }

  getIngredients() {
    return this.props.currentRecipe.ingredients.map((ingredient, index) => (
      <div className="record" key={index}>
        <div className="key">{ingredient}</div>
      </div>
    ));
  }

  favouriteMyRecipe = () => {
    this.props.favouriteRecipe(
      this.props.currentRecipe,
      !this.props.currentRecipe.isFavourite
    );
  };

  rateMyRecipe = rating => {
    this.props.rateRecipe(this.recipeId, rating);
  };

  render() {
    const recipe = this.props.currentRecipe;
    const recipeName = recipe.name || this.props.match.params.name;
    return (
      <div className="recipe-detail-container">
        <div className="row headline-banner">
          <div className="column sm-12 lg-8">
            <h2 className="name ellipsis">{recipeName}</h2>
            <h4 className="headline ellipsis">{recipe.headline}</h4>
          </div>
          <div className="column sm-12 lg-4">
            <div className="row">
              <div className="column sm-1 lg-12">
                <div className="fave-block">
                  <FavouriteMe
                    class="pdp-fav"
                    onFavouriting={this.favouriteMyRecipe}
                    isFavourite={!!recipe.isFavourite}
                  />
                </div>
              </div>
              <div className="column sm-11 lg-12">
                <div className="rate-block">
                  <RateMe
                    class="pdp-rating"
                    currentRating={recipe.currentRating}
                    onRating={this.rateMyRecipe}
                  />
                  <div className="avg-rating">
                    Rating:
                    <span className="bold">
                      {recipe.newRating || recipe.rating}
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {recipe &&
          recipe.id && (
            <div className="row">
              <Panel>
                <div className="row">
                  <div className="column sm-12 lg-8 relative-column">
                    <img
                      className="image-banner"
                      src={recipe.image}
                      alt={'Image-' + recipe.name}
                    />
                    <Badge
                      class="pdp-prep-time"
                      type="info"
                      title={`Prep time: ${formatPrepTime(
                        recipe.time
                      )} minutes`}
                    />
                  </div>
                  <div className="column sm-12 lg-4 details-column">
                    <Panel type="box">{recipe.description}</Panel>
                    <Panel type="box" title="Nutrition Values">
                      {this.getNutritionValue()}
                    </Panel>
                    <Panel type="box" title="Ingredients required">
                      {this.getIngredients()}
                    </Panel>
                  </div>
                </div>
              </Panel>
            </div>
          )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  currentRecipe: state.recipes.currentRecipe,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      fetchCurrentRecipe,
      favouriteRecipe,
      rateRecipe,
    },
    dispatch
  );

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RecipeDetailComponent)
);
