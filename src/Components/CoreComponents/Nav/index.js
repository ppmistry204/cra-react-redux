import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import regularLogo from '../../../Assets/logos/halalbox-logo.jpeg';
const horizontalLogo = regularLogo;
const Nav = props => {
  const menus = [
    { name: 'HoMe', linkTo: '/' },
    { name: 'DemoGrid', linkTo: '/demoGrid' },
    { name: 'Components', linkTo: '/components' },
  ];
  return (
    <div className="nav">
      <div className="content">
        <Link to="/">
          <img
            className="logo-horizontal"
            src={horizontalLogo}
            alt="halalbox-logo"
          />
          <img className="logo-small" src={regularLogo} alt="halalbox-logo" />
        </Link>
        <ul className="menu-list">
          {menus.map((menu, index) => {
            return (
              <li key={index}>
                <Link to={menu.linkTo}>{menu.name}</Link>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default withRouter(Nav);
