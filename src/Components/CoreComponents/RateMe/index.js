// RateMe Props
//
// currentRating:     string,   current rating of recipe
// onRating:          cb,       cb to call when rating the recipe.
// class (optional):  string,   class to override default CSS

import React from 'react';
const ratingArray = [
  { rating: 1, tip: '1. Very bad', icon: 'far fa-angry' },
  { rating: 2, tip: '2. Bad', icon: 'far fa-frown' },
  { rating: 3, tip: '3. Ok', icon: 'far fa-meh' },
  { rating: 4, tip: '4. Good', icon: 'far fa-smile' },
  { rating: 5, tip: '5. Very good', icon: 'far fa-grin-hearts' },
];
const RateMe = props => {
  return (
    <div className={`rate-me ${props.class || ''}`}>
      {ratingArray.map(r => {
        return (
          <i
            className={`${r.icon} ${
              r.rating <= props.currentRating ? 'selected' : ''
            }`}
            key={r.rating}
            title={r.tip}
            onClick={() => props.onRating(r.rating)}
          />
        );
      })}
      {props.currentRating && (
        <i
          className="fas fa-times-circle clear-rating"
          title="Clear rating"
          onClick={() => props.onRating(null)}
        />
      )}
    </div>
  );
};

export default RateMe;
