// Panel Props
//
// title (optional) :       string      , Title for Panel
// class (optional) :       {}          , class to override default CSS
// type (optional)  :       clean/box   , type of Panel (default clean)

import React from 'react';

const Panel = props => {
  let classes = 'panel-box';
  if (props.type) classes += ` ${props.type}`;
  if (props.class) classes += ` ${props.class}`;
  return (
    <div className={classes}>
      {props.title && <h5 className="panel-title">{props.title}</h5>}
      <div className="panel-content">{props.children}</div>
    </div>
  );
};

export default Panel;
