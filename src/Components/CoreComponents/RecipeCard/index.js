// RecipeCard Props
//
// recipe:      {}      , recipe details
// onFavourite: fn      , cb to favourite/unfavourite recipe
// onRating:    fn      , cb to rate recipe

import React from 'react';
import { Link } from 'react-router-dom';
import { replaceSpaceInString, formatPrepTime } from '../../../Utils/utils';
import Badge from '../Badge/index';
import FavouriteMe from '../FavouriteMe/index';
import RateMe from '../RateMe/index';

const RecipeCard = props => {
  const { recipe, onFavouriting, onRating } = props;
  const linkTo = `/recipe/${replaceSpaceInString(recipe.name)}/${recipe.id}`;
  const prepTime = formatPrepTime(recipe.time);
  const favouriting = () => {
    onFavouriting(recipe, !recipe.isFavourite);
  };
  const rateMyRecipe = rating => {
    onRating(recipe.id, rating);
  };
  return (
    <div className="recipe-card">
      <Link to={linkTo} className="link">
        <h5 className="title bold ellipsis" title={recipe.name}>
          {recipe.name}
        </h5>
        <img
          className="image"
          src={recipe.thumb}
          alt={'Image-' + recipe.name}
        />
        {recipe.calories && (
          <Badge
            class="calorie"
            type="alert"
            title={'Cal: ' + recipe.calories}
          />
        )}
        {prepTime && (
          <Badge
            class="prep-time"
            type="info"
            title={`Prep time: ${prepTime} minutes`}
          />
        )}
      </Link>
      <div className="info">
        <div className="row">
          <div className="column sm-6">
            <h6 className="medium ellipsis" title={recipe.name}>
              {recipe.name}
            </h6>
          </div>
          <div className="column sm-6">
            <h6 className="ellipsis" title={recipe.headline}>
              {recipe.headline}
            </h6>
          </div>
        </div>
        <div className="row">
          <div className="column sm-11">
            <RateMe
              class="plp-rating"
              currentRating={recipe.currentRating}
              onRating={rateMyRecipe}
            />
            <div className="avg-rating">
              Rating:{' '}
              <span className="bold">{recipe.newRating || recipe.rating}</span>
            </div>
          </div>
          <div className="column sm-1">
            <FavouriteMe
              class={'plp-fav'}
              isFavourite={!!recipe.isFavourite}
              onFavouriting={favouriting}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default RecipeCard;
