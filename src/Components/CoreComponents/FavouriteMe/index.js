// FavouriteMe Props
//
// onFavouriting:       cb      , CB for favouriting the recipe
// isFavourite:         bool    , flag to set the favourite
// class (optional):    string  , class to override default CSS

import React from 'react';

const FavouriteMe = props => {
  const { onFavouriting, isFavourite } = props;
  return (
    <div
      className={`icon-fav ${props.class ? props.class : ''}`}
      onClick={onFavouriting}>
      <i className={`fa-heart ${isFavourite ? 'fas' : 'far'}`} />
    </div>
  );
};

export default FavouriteMe;
