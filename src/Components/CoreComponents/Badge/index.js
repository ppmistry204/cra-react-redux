// Badge Props
//
// type:                string,   type of badge: info, alert, success.
// (default to info)
// title:               string,   text to be displayed in badge.
// class (optional):    string  , class to override default CSS

import React from 'react';

const Badge = props => {
  const classNames = ['badge'];
  if (props.type) classNames.push('badge-' + props.type);
  if (props.class) classNames.push(props.class);
  return (
    props.title && <div className={classNames.join(' ')}>{props.title}</div>
  );
};

export default Badge;
