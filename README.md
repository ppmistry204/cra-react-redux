# CRA + redux boilerplate

This is cloned from CRA (Create react app).

We will be using 
- [React Bootstrap | https://github.com/react-bootstrap/react-bootstrap] for basic components. ~~(OR [react-toolbox | https://github.com/react-toolbox/react-toolbox])~~
- [Recharts | https://github.com/recharts/recharts] for charts. (OR [Victory | https://formidable.com/open-source/victory/] )

**As and when require, will also add/update other packages.**
